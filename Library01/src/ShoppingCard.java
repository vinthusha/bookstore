import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ShoppingCard extends JFrame {

	private JPanel contentPane;
	private JTextField txtShoppingCard;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShoppingCard frame = new ShoppingCard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ShoppingCard() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtShoppingCard = new JTextField();
		txtShoppingCard.setBounds(133, 77, 86, 20);
		contentPane.add(txtShoppingCard);
		txtShoppingCard.setColumns(10);

		JLabel lblNewLabel = new JLabel("Product");
		lblNewLabel.setBounds(42, 80, 46, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Shopping Card");
		lblNewLabel_1.setBounds(152, 22, 86, 20);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Quantity");
		lblNewLabel_2.setBounds(42, 115, 46, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Price");
		lblNewLabel_3.setBounds(42, 154, 46, 14);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("Total");
		lblNewLabel_4.setBounds(42, 191, 46, 14);
		contentPane.add(lblNewLabel_4);

		textField = new JTextField();
		textField.setBounds(133, 112, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(133, 151, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(133, 188, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		JButton btnNewButton = new JButton("Check Out");
		btnNewButton.setBounds(278, 227, 89, 23);
		contentPane.add(btnNewButton);
	}

}
